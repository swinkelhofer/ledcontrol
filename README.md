# LEDControl

An Android app that let's you control multiple LED strips that run with [WiFi-Support](https://gitlab.com/swinkelhofer/led_wifi_control).

<a href="https://gitlab.com/api/v4/projects/11704072/jobs/artifacts/master/raw/app-debug.apk?job=build" download>Download latest version</a>

<img src="screenshot.jpg"  width="30%" height="30%">

## Add new LED control

 * Click on the "+" button
 * Set location (Name like "under the bed") and the host (hostname/ip of the LED controller), default port 2390 is assumed

