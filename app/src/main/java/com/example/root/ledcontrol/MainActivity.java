package com.example.root.ledcontrol;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;
import android.support.design.widget.Snackbar;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements RecyclerItemTouchHelper.RecyclerItemTouchHelperListener, SeekBar.OnSeekBarChangeListener {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private CoordinatorLayout coordinatorLayout;
    private SeekBar seekBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new ItemAdapter();
        ((ItemAdapter)mAdapter).mDataset = new ArrayList<Triplet<Integer,String,String>>();
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        mRecyclerView.setAdapter(mAdapter);
        coordinatorLayout = findViewById(R.id.coordinator_layout);
        seekBar = findViewById(R.id.seekBar);
        seekBar.setMax(16);
        seekBar.setOnSeekBarChangeListener(this);
        seekBar.setProgress(16);
        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(mRecyclerView);
        try {
            readSettings();
        } catch (Exception e) {
            e.printStackTrace();
        }
        findViewById(R.id.add_device).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddNewDialogFragment dialog = new AddNewDialogFragment(new AddNewDialogFragment.callbackInterface() {
                    @Override
                    public void onSuccess(Dialog dialog) {
                        String hostname = ((TextView)dialog.findViewById(R.id.hostname)).getText().toString();
                        String location = ((TextView)dialog.findViewById(R.id.location)).getText().toString();
                        Snackbar snackbar = null;
                        if(location.trim().isEmpty() )
                            snackbar = Snackbar.make(coordinatorLayout, "Please provide a location or common name for the Controller", Snackbar.LENGTH_LONG);
                        else if (hostname.trim().isEmpty())
                            snackbar = Snackbar.make(coordinatorLayout, "Please provide an IP or a hostname for the Controller", Snackbar.LENGTH_LONG);
                        if(hostname.trim().isEmpty() || location.trim().isEmpty()) {
                            snackbar.setActionTextColor(Color.YELLOW);
                            snackbar.show();
                            dialog.cancel();
                            return;
                        }
                        Triplet<Integer, String,String> entry = new Triplet<>(1000+((ItemAdapter)mAdapter).mDataset.size(), location, hostname);

                        ((ItemAdapter)mAdapter).addItem(entry);

                        try {
                            writeSettings();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                });
                dialog.show(getSupportFragmentManager(), "AddNewDialogFragment");
            }
        });

           /*     .setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                AddNewDialogFragment dialog = new AddNewDialogFragment(new AddNewDialogFragment.callbackInterface() {
                    @Override
                    public void onSuccess(Dialog dialog) {
                        String hostname = ((TextView)dialog.findViewById(R.id.hostname)).getText().toString();
                        String location = ((TextView)dialog.findViewById(R.id.location)).getText().toString();
                        Snackbar snackbar = null;
                        if(location.trim().isEmpty() )
                            snackbar = Snackbar.make(coordinatorLayout, "Please provide a location or common name for the Controller", Snackbar.LENGTH_LONG);
                        else if (hostname.trim().isEmpty())
                            snackbar = Snackbar.make(coordinatorLayout, "Please provide an IP or a hostname for the Controller", Snackbar.LENGTH_LONG);
                        if(hostname.trim().isEmpty() || location.trim().isEmpty()) {
                            snackbar.setActionTextColor(Color.YELLOW);
                            snackbar.show();
                            dialog.cancel();
                            return;
                        }
                        Triplet<Integer, String,String> entry = new Triplet<>(1000+((ItemAdapter)mAdapter).mDataset.size(), location, hostname);

                        ((ItemAdapter)mAdapter).addItem(entry);

                        try {
                            writeSettings();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                });
                dialog.show(getSupportFragmentManager(), "AddNewDialogFragment");
                return false;
            }
        });*/
    }

    private void writeSettings() throws IOException {
        FileOutputStream outputStream = openFileOutput("settings.sav", Context.MODE_PRIVATE);
        ObjectOutputStream objOutputStream = new ObjectOutputStream(outputStream);
        for (Object obj : ((ItemAdapter)this.mAdapter).mDataset) {

            objOutputStream.writeObject(obj);
            objOutputStream.reset();
        }
        objOutputStream.close();
    }

    private void readSettings() throws ClassNotFoundException, IOException {
        FileInputStream inputStream = openFileInput("settings.sav");
        ObjectInputStream objInputStream = new ObjectInputStream(inputStream);
        try {
            while (inputStream.available() != -1) {
                Triplet<Integer, String, String> entry = (Triplet<Integer, String, String>) objInputStream.readObject();
                ((ItemAdapter)mAdapter).addItem(entry);
            }
        } catch (EOFException ex) {
            ex.printStackTrace();
        }
    }


    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof ItemAdapter.ItemHolder) {
            final Triplet<Integer, String, String> deletedItem = ((ItemAdapter)mAdapter).mDataset.get(viewHolder.getAdapterPosition());
            final int deletedIndex = viewHolder.getAdapterPosition();
            String name = deletedItem.getSecond();

            ((ItemAdapter)mAdapter).removeItem(viewHolder.getAdapterPosition());
            Snackbar snackbar = Snackbar.make(coordinatorLayout, name + " removed", Snackbar.LENGTH_LONG);
            snackbar.setAction("UNDO", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((ItemAdapter)mAdapter).restoreItem(deletedItem, deletedIndex);
                    try {
                        writeSettings();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            snackbar.setActionTextColor(Color.RED);
            snackbar.show();
            try {
                writeSettings();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        menu.findItem(R.id.all_on).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                for(int i = 0; i < ((ItemAdapter)mAdapter).mDataset.size(); ++i) {
                    ((ItemAdapter.ItemHolder)mRecyclerView.findViewHolderForAdapterPosition(i)).mSwitch.setChecked(true);
                }
                return false;
            }
        });
        menu.findItem(R.id.all_off).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                for(int i = 0; i < ((ItemAdapter)mAdapter).mDataset.size(); ++i) {
                    ((ItemAdapter.ItemHolder)mRecyclerView.findViewHolderForAdapterPosition(i)).mSwitch.setChecked(false);
                }
                return false;
            }
        });
        return true;
    }

    public static int udpDialog(int code, String address) {
        Runnable r = new UDPBackground(code, address, true);
        new Thread(r).start();
        while(! ((UDPBackground) r).isReady())
            continue;
        return ((UDPBackground) r).getValue();
    }
    public static void udpSend(int code, String address) {
        Runnable r = new UDPBackground(code, address, false);
        new Thread(r).start();
        while(! ((UDPBackground) r).isReady())
            continue;
    }


    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        ((ItemAdapter)mAdapter).brightness = seekBar.getProgress();
        /*for(int i = 0; i < ((ItemAdapter)mAdapter).mDataset.size(); ++i) {
            if(((ItemAdapter.ItemHolder)mRecyclerView.findViewHolderForAdapterPosition(i)).mSwitch.isChecked()) {
                udpSend(((ItemAdapter)mAdapter).brightness, ((ItemAdapter)mAdapter).mDataset.get(i).getThird());
            }
        }*/
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        return;
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        ((ItemAdapter)mAdapter).brightness = seekBar.getProgress();
        for(int i = 0; i < ((ItemAdapter)mAdapter).mDataset.size(); ++i) {
            if(((ItemAdapter.ItemHolder)mRecyclerView.findViewHolderForAdapterPosition(i)).mSwitch.isChecked()) {
                udpSend(((ItemAdapter)mAdapter).brightness, ((ItemAdapter)mAdapter).mDataset.get(i).getThird());
            }
        }
    }
}
