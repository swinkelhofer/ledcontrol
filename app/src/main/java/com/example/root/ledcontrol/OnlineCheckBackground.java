package com.example.root.ledcontrol;

import android.os.Handler;
import android.widget.SeekBar;

public class OnlineCheckBackground extends Thread {

    private ItemAdapter.ItemHolder holder;
    private ItemAdapter adapter;
    private MainActivity ctx;
    String hostname;
    private Handler handler;

    public OnlineCheckBackground(MainActivity ctx, ItemAdapter adapter, ItemAdapter.ItemHolder holder, String hostname, Handler handler) {
        this.adapter = adapter;
        this.holder = holder;
        this.hostname = hostname;
        this.handler = handler;
        this.ctx = ctx;
    }

    @Override
    public void run() {
        int ret = MainActivity.udpDialog(-1, hostname);
        if (ret > 0) {
            holder.mSwitch.setBackgroundColor(0xFFFFFFFF);
            holder.mSwitch.setTextColor(0xFF333333);
            holder.mSwitch.setChecked(true);
            adapter.brightness = ret;
            ((SeekBar)ctx.findViewById(R.id.seekBar)).setProgress(ret);
        } else if (ret == 0) {
            holder.mSwitch.setBackgroundColor(0xFFFFFFFF);
            holder.mSwitch.setTextColor(0xFF333333);
            holder.mSwitch.setChecked(false);
        } else {
            holder.mSwitch.setBackgroundColor(0xFF632929);
            holder.mSwitch.setTextColor(0xFFFFFFFF);
        }
        handler.postDelayed(this, 2000);
    }

}
