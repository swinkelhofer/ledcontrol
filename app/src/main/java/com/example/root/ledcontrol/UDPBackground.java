package com.example.root.ledcontrol;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class UDPBackground implements Runnable {
    private volatile int value = -255;
    private volatile int code;
    private boolean should_return;
    private String address;

    public UDPBackground(int code, String address, boolean should_return) {
        this.code = code;
        this.address = address;
        this.should_return = should_return;
    }
    @Override
    public void run() {
        DatagramSocket ds = null;
        String ret = null;
        try {
            ds = new DatagramSocket();
            InetAddress serverAddr = InetAddress.getByName(this.address);
            DatagramPacket dp;
            String message;
            if(this.code > 0)
                message = Integer.toString(this.code * 64 - 1);
            else
                message = Integer.toString(this.code);
            dp = new DatagramPacket(message.getBytes(), message.length(), serverAddr, 2390);
            ds.send(dp);
            if(! this.should_return) {
                value = -1;
            } else {
                byte[] retMsg = new byte[10];
                dp = new DatagramPacket(retMsg, retMsg.length);
                ds.receive(dp);
                ret = new String(retMsg, 0, dp.getLength());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (ds != null) {
                ds.close();
                try {
                    //value = (int)(Math.log((double)Integer.parseInt(ret)+1d)/Math.log(2d));
                    value = (Integer.parseInt(ret) + 1) / 64;
                } catch (Exception e) {
                    value = -1;
                }
            } else {
                value = -1;
            }
        }
    }

    public int getValue() {
        return value;
    }

    public boolean isReady() {
        if(value == -255)
            return false;
        else
            return true;
    }
}
