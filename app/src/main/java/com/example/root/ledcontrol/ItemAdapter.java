package com.example.root.ledcontrol;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Switch;

import java.util.List;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ItemHolder> implements CompoundButton.OnCheckedChangeListener {
    public List<Triplet<Integer,String,String>> mDataset;
    public int brightness;
    private Context ctx;


    public static class ItemHolder extends RecyclerView.ViewHolder {
        public Switch mSwitch;
        public ItemHolder(Switch v) {
            super(v);
            mSwitch = v;
        }
    }


    @Override
    public ItemAdapter.ItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ctx = parent.getContext();
        Switch v = new Switch(ctx);
        ItemHolder vh = new ItemHolder(v);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT
        );
        v.setLayoutParams(params);
        final int px = (int) (20 * ctx.getResources().getDisplayMetrics().density + 0.5f);
        v.setPadding(px, px, px, px);
        v.setBackgroundColor(0xFFFFFFFF);
        return vh;
    }

    @Override
    public void onBindViewHolder(ItemHolder holder, final int position) {
        holder.mSwitch.setText(mDataset.get(position).getSecond());
        holder.mSwitch.setId(mDataset.get(position).getFirst());
        holder.mSwitch.setOnCheckedChangeListener(ItemAdapter.this);
        Handler handler = new Handler();
        OnlineCheckBackground runnableCode = new OnlineCheckBackground((MainActivity)this.ctx,this, holder, mDataset.get(position).getThird(), handler);
        handler.post(runnableCode);

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void removeItem(int position) {
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    public void addItem(Triplet<Integer, String, String> entry) {
        mDataset.add(entry);
        notifyItemInserted(mDataset.size()-1);
    }

    public void restoreItem(Triplet<Integer, String, String> entry, int position) {
        mDataset.add(position, entry);
        // notify item added by position
        notifyItemInserted(position);
    }

    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        Triplet<Integer, String, String> index = null;
        for(int i = 0; i < mDataset.size(); ++i) {
            if(mDataset.get(i).getFirst() == buttonView.getId()) {
                index = mDataset.get(i);
                break;
            }
        }

        if(index == null)
            return;

        if (isChecked) {
            MainActivity.udpSend(this.brightness, index.getThird());
        } else {
            MainActivity.udpSend(0, index.getThird());
        }
    }

}
