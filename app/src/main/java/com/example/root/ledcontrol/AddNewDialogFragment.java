package com.example.root.ledcontrol;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;

public class AddNewDialogFragment extends DialogFragment {

    public interface callbackInterface {
        void onSuccess(Dialog dialog);
    }
    callbackInterface callback;

    public AddNewDialogFragment(AddNewDialogFragment.callbackInterface callback) {
        this.callback = callback;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        builder.setView(inflater.inflate(R.layout.add_new_dialog, null))
        .setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                callback.onSuccess(AddNewDialogFragment.this.getDialog());
            }
        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        AddNewDialogFragment.this.getDialog().cancel();
                    }
                });
        return builder.create();
    }
}
